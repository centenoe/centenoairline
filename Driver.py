# Edgar Centeno
# Airline project
# This project will take a list of planes from AirlineCSV.csv and organize it based on submission time
# and requested start
# This means that if a plane submitted a time early
import csv
from Time import printout

# this will open the AirlineCSV.csv file
file = open('AirlineCSV.csv', newline='')
# read the file
reader = csv.reader(file)
# skip the header of the file so I can add the appropriate variable type
header = next(reader)

# Creating airlineData list
airline_data = []

# this is filling the list from the csv file
for row in reader:
    ID = row[0]
    # A csv file automatically makes everything a string so I have to turn numbered strings into integers
    Sub_time = int(row[1])
    Req_time = int(row[2])
    Req_Dur = int(row[3])

    # row = [ID, Submission Time, Requested Start, Requested Duration]
    airline_data.append([ID, Sub_time, Req_time, Req_Dur])

# this is a test to call the function from another class
print("This is the original data list: ",airline_data)
# This sorts the list based on the submission time
airline_data.sort(key=lambda x: x[2])
print("This is the sorted list by submission time: ", airline_data)
print("---")
print("This is the actual take off times: ", printout(airline_data))
