""""
this is the print out function. It will print the planes out based on submission and start time
In this function the planes are put in a waiting list when their submission time matches the actual time
When the plane is submitted into the waiting list the submission time does not really matter
so it organizes by the desired start time.
The program will print this out in order (current time, waiting list, flying list, current duration(duration of flying plane")
in the end it will print out the actual take off times.
:param airlineprint: this will take in a list 
:return printout the actual take off times.
"""
def printout(airlineprint):
    time = 1
    current_duration = 0
    flying = []
    waiting = []
    actual_takeoff = []

    # the range is the length of the list given
    for i in range(25):
        # airlineprint[i][1]:
        # [i] gets the plane
        # [1] gets the submission time of the plane
        while i < len(airlineprint):
            if airlineprint[i][1] == time:
                # This is the waiting list based in real time
                waiting.append(airlineprint[i])
                waiting.sort(key=lambda x: (x[2]))
            i=i+1
        # This will check who is flying
        for t in range(len(airlineprint)-1):
            # If there is nothing in the flying list and the plane chosen requested start is right now then remove
            # from waiting and add it to flying.
            if not flying and airlineprint[t][2] == time:
                # add this plane to the flying list
                flying.append(airlineprint[t])
                # delete the current plane as it is flying right now
                waiting.remove(airlineprint[t])
                # if there is only one plane in the waiting list then give it its final time.
                if len(waiting) == 1:
                    waiting[0][2] = time + 1

        # The print is here because if it is below the flying for loop the flying variable will have been deleted
        print("Current Time:", time, waiting,  "currently flying:", flying, "Current Duration ", current_duration)

        # If the plane that is currently flying has a duration less than or equal to the current duration then
        # set the flying plane into the actual take off time
        # clear the flying list so that it is empty
        # set current_duration to zero
        for y in range(len(flying)):
            if flying[y][3] == current_duration:
                # this creates the actual take off times
                actual_takeoff.append(flying[y])
                # this makes sure that the flying list is empty
                flying.clear()
                print("flying lane clear")
                current_duration = 0
                # the duration pushed back the start times, make the start time the time that is clear.
                waiting[y][2] = time+1
                # is there is only one plane left in the waiting line then just send it straight to actualtakeoff
                if len(waiting) == 1:
                    actual_takeoff.append(waiting[0])
        # keep track of the current duration
        current_duration = current_duration+1
        # keep track of time
        time = time+1
    # prints out the start and end times of every plane
    for i in range(len(actual_takeoff)):
        print(actual_takeoff[i][0], "start time:", actual_takeoff[i][2], "| end time:", actual_takeoff[i][2] + actual_takeoff[i][3])
    # returns the actual take off time list.
    return actual_takeoff

