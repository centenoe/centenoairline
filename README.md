Name: Edgar Centeno
Project: Airline Simulator

This project will simulate an airport aircraft take-off times using the language python. 
In order for the simulation to take place, it will need a list of ID, submission times, requested start, and requested duration from the planes. 
It will then sort the aircraft take-off based on the requested start time according to the submission time in real time. 
Since the requested duration can be during someone else's requested start time, the start time for the aircraft will be changed to a later start time. 
The queue of planes will be printed in real time and will have the actual take-off time printed at the end.
The program will print this out in order (current time, waiting list, flying list, current duration(duration of flying plane") it will print this
line for every time has passed. To be clear time is incraminted by whole integers(1,2,3...)
The last line to be printed is the list of actual take off times. 
The program uses a flying, waiting and actual_take off lists to organize and printout proporly. 
    
The software design document is called AirlineSimulatorSDD.xml (PDF included)  
The list of planes comes from AirlineCSV.csv
csv is used to organize the list of planes to better manipulate them. 